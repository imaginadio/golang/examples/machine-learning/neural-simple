package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"github.com/goml/gobrain"
)

const inputLength = 4

func main() {
	patterns := [][][]float64{
		{{0, 0, 0, 0}, {0}},
		{{0, 1, 0, 0}, {0}},
		{{0, 0, 0, 1}, {0}},
		{{1, 0, 0, 0}, {0}},
		{{0, 0, 1, 0}, {0}},
		{{1, 1, 1, 1}, {1}},
		{{1, 1, 0, 0}, {1}},
		{{0, 1, 1, 0}, {1}},
		{{0, 0, 1, 1}, {1}},
		{{1, 0, 1, 0}, {1}},
		{{1, 0, 0, 1}, {1}},
		{{0, 1, 0, 1}, {1}},
		{{1, 1, 1, 0}, {1}},
		{{0, 1, 1, 1}, {1}},
		{{1, 1, 0, 1}, {1}},
		{{1, 0, 1, 1}, {1}},
	}

	ff := &gobrain.FeedForward{}
	ff.Init(inputLength, 2, 1)
	ff.Train(patterns, 1000, 0.6, 0.4, false)

	input := bufio.NewReader(os.Stdin)
inputPoint:
	for {
		fmt.Print(">>")
		line, err := input.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		inputNumbers := strings.Split(strings.TrimSpace(line), " ")
		if len(inputNumbers) != inputLength {
			fmt.Println("Wrong input! Example: 1 1 0 1")
			continue
		}

		in := make([]float64, inputLength)
		for i, str := range inputNumbers {
			converted, err := strconv.Atoi(str)
			if err != nil {
				fmt.Println("Wrong input! error:", err)
				break inputPoint
			}
			in[i] = float64(converted)
		}

		out := ff.Update(in)
		fmt.Println(out)
	}
}
